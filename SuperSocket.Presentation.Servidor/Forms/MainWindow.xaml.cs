﻿using SocketUniversal.Domain;
using SocketUniversal.Sockets;
using SocketUniversal.SocketServidor.Util;
using SocketUniversal.Util;
using SocketUniversal.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SuperSocket.Presentation.Servidor.Forms
{
    /// <summary>
    /// Lógica interna para MainWindow.xaml
    /// codebehind da tela principal da aplicação
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// armazena o usuário que detém função de servidor
        /// </summary>
        private User userServer;
        /// <summary>
        /// armazena o usuário cliente (caso a classe esteja no modo cliente)
        /// </summary>
        private User userClient;
        /// <summary>
        /// armazena classe server ou client
        /// </summary>
        private TypeOperation tipoOperacao;

        private UserViewModel userVM;

        private MediaPlayer mediaPlayer;

        // métodos delegates
        private delegate void AtualizaStatusCallback(string strMensagem);

        private delegate void AtualizaStatusCallbackAlert(string strMensagem);

        private delegate void AtualizaStatusCallbackExit();

        private delegate void MessageEvent(Message message);

        public MainWindow(User userServer, User userClient=null)
        {
            InitializeComponent();

            mediaPlayer = new MediaPlayer();

            this.userServer = userServer;

            if(userClient != null)
            {
                this.userClient = userClient;
            }

            userVM = new UserViewModel();

            this.DataContext = userVM;

            desenhaMainWindow();
        }

        /// <summary>
        /// método para configurar MainWindow conforme tipo de usuário
        /// </summary>
        /// <param name="client">Objeto cliente</param>
        private void desenhaMainWindow()
        {
            TypeOperation.StatusChanged += new StatusChangedEventHandler(mainServidor_StatusChanged);
            TypeOperation.StatusChangedAlert += new StatusChangedEventHandlerAlert(mainServidor_StatusChangedAlert);
            TypeOperation.StatusChangedExit += new StatusChangedEventHandlerExit(mainServidor_StatusChangedExit);
            TypeOperation.MessageSender += new MessageEventHandler(mainServidor_Message);

            if (userClient != null)
            {
                this.Title = "SuperSocket - Cliente";
                btExpulsar.Visibility = Visibility.Collapsed;
                DockPanelBottom.Visibility = Visibility.Collapsed;
                tbUsuario.Text = "Bem-vindo cliente " + userClient.NomeUsuario;
                tbIP.Text = "IP LOCAL: " + ((IPEndPoint)userServer.EndPoint).Address + ":" + ((IPEndPoint)userServer.EndPoint).Port;

                try
                {
                    tipoOperacao = new Client(userServer, userClient);
                } catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                this.Title = "SuperSocket - Servidor";
                btExpulsar.Visibility = Visibility.Visible;
                tbUsuario.Text = "Bem-vindo administrador " + userServer.NomeUsuario;
                tbIP.Text = "IP LOCAL: " + ((IPEndPoint)userServer.EndPoint).Address + ":" + ((IPEndPoint)userServer.EndPoint).Port;

                try
                {
                    tipoOperacao = new Server(userServer);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public void mainServidor_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            // Chama o método que atualiza txtLogs
            this.txtLogs.Dispatcher.Invoke(new AtualizaStatusCallback(this.AtualizaStatus), new object[] { e.EventMessage });
        }

        public void mainServidor_StatusChangedAlert(object sender, StatusChangedEventArgs e)
        {
            // Chama o método que atualiza o formulário com alertas
            this.Dispatcher.Invoke(new AtualizaStatusCallbackAlert(this.AlertaMsgBox), e.EventMessage);
        }

        public void mainServidor_StatusChangedExit(object sender, StatusChangedEventArgs e)
        {
            // chama método para sair da aplicação
            this.Dispatcher.Invoke(new AtualizaStatusCallbackExit(this.exit));
        }

        public void mainServidor_Message(object sender, MessageEventArgs e)
        {
            // chama o método ao receber mensagem
            this.Dispatcher.Invoke(new MessageEvent(ReceiveMessage), e.Message);   
        }

        /// <summary>
        /// método para processar message
        /// podendo criar chatbox, se não há chatbox criado para a conversa
        /// </summary>
        /// <param name="message">objeto mensagem recebida</param>
        private void ReceiveMessage(Message message)
        {
            // verifica o tipo de mensagem
            if (message.GetType() == typeof(MessageSendMessage))
            {
                MessageSendMessage messageS = message as MessageSendMessage;
                // verifica se já há chatbox com determinado usuário
                ChatBox window = IsWindowOpen(messageS.From);

                // verifica se não há chatbox do usuário
                if (window == null)
                {
                    ChatBox chatBox = new ChatBox()
                    {
                        From = userClient != null ? userClient : userServer,
                        To = messageS.From,
                        TipoOperacao = tipoOperacao
                    };
                    chatBox.Show();
                    chatBox.ReceiveMessage(messageS.MessageText);
                }
                else
                {
                    window.ReceiveMessage(messageS.MessageText);
                    if (window.WindowState == WindowState.Minimized)
                    {
                        window.WindowState = WindowState.Normal;
                    }

                    chatBoxActivate(window);
                }
                // toca o som
                mediaPlayer.Open(new Uri(@"Resources\Sounds\type.wav", UriKind.Relative));
                mediaPlayer.Play();
            }
            else if (message.GetType() == typeof(MessageSendFile))
            {
                MessageSendFile messageS = message as MessageSendFile;
                ChatBox window = IsWindowOpen(messageS.From);

                if (window == null)
                {
                    ChatBox chatBox = new ChatBox()
                    {
                        From = userClient != null ? userClient : userServer,
                        To = messageS.From,
                        TipoOperacao = tipoOperacao
                    };
                    chatBox.Show();
                    chatBox.ReceiveFile(messageS.FileName, messageS.File);
                }
                else
                {
                    window.ReceiveFile(messageS.FileName, messageS.File);
                    if (window.WindowState == WindowState.Minimized)
                    {
                        window.WindowState = WindowState.Normal;
                    }

                    chatBoxActivate(window);
                }
                mediaPlayer.Open(new Uri(@"Resources\Sounds\type.wav", UriKind.Relative));
                mediaPlayer.Play();
            }
            else if (message.GetType() == typeof(MessageLogoff))
            {
                MessageLogoff messageS = message as MessageLogoff;

                if(userClient != null)
                {
                    // verifica se o usuário que saiu foi o servidor
                    // neste caso, aplicativo será fechado
                    if (messageS.UserLogoff.Opcao == OpcoesUserEnum.Servidor)
                    {
                        MessageBox.Show("O servidor saiu.\nO aplicativo será fechado.");
                        this.exit();
                        return;
                    }
                }

                ChatBox window = IsWindowOpen(messageS.UserLogoff);

                if (window != null)
                {
                    window.ReceiveMessageLogoff();
                }
            }
            else if (message.GetType() == typeof(MessageSendList))
            {
                MessageSendList messageS = message as MessageSendList;
                AtualizaListBox(messageS.ListUsers);
            }
        }

        /// <summary>
        /// verifica se a janela chatbox foi criado para uma conversa com usuário
        /// </summary>
        /// <param name="userTo">usuário pra conversar</param>
        /// <returns></returns>
        public static ChatBox IsWindowOpen(User userTo)
        {
            return Application.Current.Windows.OfType<ChatBox>().Where(w => w.To.Equals(userTo)).FirstOrDefault() as ChatBox;
        }


        /// <summary>
        /// evento para conversar com determinado usuário
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btConversar_Click(object sender, RoutedEventArgs e)
        {
            User userFrom = userClient != null ? userClient : userServer;
            User userSelected = listBoxConnections.SelectedItem as User;
            if (userSelected == null || userFrom.Equals(userSelected))
            {
                return;
            }

            ChatBox window = IsWindowOpen(userSelected);
            if (window == null)
            {
                ChatBox chatBox = new ChatBox()
                {
                    From = userFrom,
                    To = userSelected,
                    TipoOperacao = tipoOperacao
                };
                chatBox.Show();
            }
            else
            {
                chatBoxActivate(window);
            }

        }

        /// <summary>
        /// método para focar chatbox aberto
        /// e habilitar (se estiver inabilitado)
        /// </summary>
        /// <param name="chatBox"> ChatBox </param>
        private static void chatBoxActivate(ChatBox chatBox)
        {
            if (chatBox == null)
            {
                return;
            }
            if (chatBox.WindowState == WindowState.Minimized)
            {
                chatBox.WindowState = WindowState.Normal;
            }
            chatBox.Activate();
            chatBox.Topmost = true;  // important
            chatBox.Topmost = false; // important
            chatBox.txtConversa.Focus();
        }

        /// <summary>
        /// atualiza textbox com os logs
        /// </summary>
        /// <param name="strMensagem"></param>
        private void AtualizaStatus(string strMensagem)
        {
            // Atualiza o logo com mensagens
            txtLogs.AppendText(DateTime.Now.ToString(@"dd/MM/yyyy HH:mm:ss: ") + strMensagem + "\n");
            txtLogs.ScrollToEnd();
        }

        /// <summary>
        /// recebe mensagens para MessageBox
        /// </summary>
        /// <param name="strMensagem"></param>
        private void AlertaMsgBox(string strMensagem)
        {
            // Atualiza o logo com mensagens
            MessageBox.Show(strMensagem);
        }

        /// <summary>
        /// atualiza listbox com usuários logados
        /// </summary>
        /// <param name="listUsers"></param>
        private void AtualizaListBox(List<User> listUsers)
        {
            int countBefore = listBoxConnections.Items.Count;
            // atualiza viewmodel
            userVM.UpdateList(listUsers);

            // percorre usuários para verifica se há um chatbox aberto
            foreach(User user in listUsers)
            {
                ChatBox c = IsWindowOpen(user);
                // reabilita chatbox
                if (c != null)
                {
                    c.ReceiveMessageLogin();
                }
            }

            int countAfter = listUsers.Count;
            if(countAfter > countBefore && countBefore > 0)
            {
                mediaPlayer.Open(new Uri(@"Resources\Sounds\online.wav", UriKind.Relative));
                mediaPlayer.Play();
            }
        }

        /// <summary>
        /// método para fechar o aplicativo
        /// </summary>
        private void exit()
        {
            tipoOperacao.Exit();
            Environment.Exit(0);
        }

        /// <summary>
        /// ao fechar a janela
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form_closed(object sender, EventArgs e)
        {
            this.exit();
        }

        /// <summary>
        /// método para expulsar usuários
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btExpulsar_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// evento ao clicar duas vezes em um usuário
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxConnections_DoubleClick(object sender, EventArgs e)
        {
            btConversar_Click(sender, null);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.exit();
        }
    }
}
