﻿using SocketUniversal.Domain;
using SocketUniversal.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SuperSocket.Presentation.Servidor.Forms
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainLogin : Window
    {
        /// <summary>
        /// armazena o tipo de opção (servidor/cliente)
        /// </summary>
        public OpcoesUserEnum opcoesForm;

        public MainLogin()
        {
            InitializeComponent();
            opcoesForm = OpcoesUserEnum.Cliente;
            rbCliente.IsChecked = true;
        }

        /// <summary>
        /// evento: ocorre quando o elemento é apresentado, processado e pronto para interação
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainLogin_Load(object sender, RoutedEventArgs e)
        {
            txtIP.Text = TypeOperation.LocalIPAddress().ToString();
        }

        /// <summary>
        /// evento que permite arrastar a janela (window) com o mouse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmLogin_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        /// <summary>
        /// evento ao clicar no botão entrar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btEntrar_Click(object sender, RoutedEventArgs e)
        {
            // verifica se o textbox txtIP está visível
            if (txtIP.Visibility == Visibility.Visible)
            {
                // verifica se os campos foram preenchidos
                if (txtUsuario.Text == "" || txtPorta.Text == "" || txtIP.Text == "")
                {
                    MessageBox.Show("Os campos Usuário, IP e Porta devem estar preenchidos", "Erro");
                    return;
                }
            } else
            {
                // verifica se os campos foram preenchidos
                if (txtUsuario.Text == "" || txtPorta.Text == "")
                {
                    MessageBox.Show("Os campos Usuário e Porta devem estar preenchidos", "Erro");
                    return;
                }
            }

            // verifica se a opção selecionada foi iniciar chat tipo servidor
            if ((bool)rbServidor.IsChecked)
            {
                // extrai usuario digitado
                string userServerString = txtUsuario.Text;

                //extrai a porta digitada
                int port = Convert.ToInt32(txtPorta.Text);

                // objeto EndPoint do servidor
                EndPoint endPointServer = new IPEndPoint(Server.LocalIPAddress(), port);

                // cria usuário tipo Client
                User userServer = new User(userServerString, OpcoesUserEnum.Servidor, endPointServer);

                // carrega nova window
                MainWindow mainWindow = new MainWindow(userServer);
                mainWindow.Show();
                this.Close();
            // verifica se a opção selecionada foi iniciar chat como cliente
            } else if ((bool)rbCliente.IsChecked)
            {
                // extrai usuario digitado
                string userClientString = txtUsuario.Text;
                // extrai o ip do servidor digitado
                string ipClient = txtIP.Text;
                // extrai a porta digitada
                int port = Convert.ToInt32(txtPorta.Text);

                // objeto EndPoint do servidor
                EndPoint endPointServer = new IPEndPoint(IPAddress.Parse(ipClient), port);

                // objeto EndPoint do servidor
                EndPoint endPointClient = new IPEndPoint(Server.LocalIPAddress(), port);
                
                // cria objeto User servidor
                User userServer = new User(userClientString, OpcoesUserEnum.Servidor, endPointServer);

                // cria objeto User cliente
                User userClient = new User(userClientString, OpcoesUserEnum.Cliente, endPointClient);

                // carrega nova window
                MainWindow mainWindow = new MainWindow(userServer, userClient);
                mainWindow.Show();
                this.Close();
            }

        }

        /// <summary>
        /// evento ao clicar no checkbox tipo de operação
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rb_select_checked(object sender, RoutedEventArgs e)
        {
            // opção de chat como servidor selecionada
            if (Convert.ToBoolean(rbServidor.IsChecked))
            {
                opcoesForm = OpcoesUserEnum.Servidor;
                lbIP.Visibility = Visibility.Collapsed;
                txtIP.Visibility = Visibility.Collapsed;
            // opção de chat como cliente selecionada
            }
            else if (Convert.ToBoolean(rbCliente.IsChecked))
            {
                opcoesForm = OpcoesUserEnum.Cliente;
                lbIP.Visibility = Visibility.Visible;
                txtIP.Visibility = Visibility.Visible;
            }
            txtUsuario.Focus();
        }

    }
}
