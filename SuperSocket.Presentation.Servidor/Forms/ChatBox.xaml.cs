﻿using Microsoft.Win32;
using SocketUniversal.Domain;
using SocketUniversal.Sockets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SuperSocket.Presentation.Servidor.Forms
{
    /// <summary>
    /// Lógica interna para ChatBox.xaml
    /// </summary>
    public partial class ChatBox : Window
    {
        /// <summary>
        /// Usuário principal
        /// </summary>
        public User From { get; set; }
        /// <summary>
        /// Usuário com quem está conversando
        /// </summary>
        public User To { get; set; }
        /// <summary>
        /// armazena objeto com o tipo de operação (Server/Client)
        /// </summary>
        public TypeOperation TipoOperacao { get; set; }

        public ChatBox()
        {
            InitializeComponent();
            rtfConversa.Document = new FlowDocument();
        }

        /// <summary>
        /// evento: ocorre quando o elemento é apresentado, processado e pronto para interação
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChatBox_Loaded(object sender, RoutedEventArgs e)
        {
            lbContato.Text = To.NomeUsuario;
            this.Title = "Conversa de " + From.NomeUsuario + " com " + To.NomeUsuario;
            txtConversa.Focus();
        }

        /// <summary>
        /// método para receber mensagem
        /// </summary>
        /// <param name="message"></param>
        public void ReceiveMessage(string message)
        {
            this.writeReceiveRtfConversa("diz: ", message);
        }
        public void ReceiveFile(string fileName, byte[] file)
        {
            this.writeReceiveRtfConversa("enviou o arquivo " + fileName);

            string extension = fileName.Split('.').Last() != null ? fileName.Split('.').Last() : "";

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = " Files .*" + extension + "|*." + extension;
            saveFileDialog.DefaultExt = extension;
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.RestoreDirectory = true;
            //saveFileDialog.CreatePrompt = true;
            saveFileDialog.Title = "Salvar arquivo";
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName != "")
            {
                try
                {
                    BinaryWriter bWrite = new BinaryWriter(File.Open(saveFileDialog.FileName, FileMode.Append));
                    bWrite.Write(file);
                    bWrite.Flush();
                    bWrite.Close();
                    bWrite.Dispose();
                    this.writeReceiveRtfConversa(": Arquivo salvo em " + saveFileDialog.FileName);
                }
                catch (IOException e)
                {
                    this.writeReceiveRtfConversa(e.Message);
                }

            } else
            {
                this.writeReceiveRtfConversa("Recebimento de arquivo cancelado.");
            }
        }

        /// <summary>
        /// método para receber mensagem de logoff
        /// e inabilitar chatbox
        /// </summary>
        public void ReceiveMessageLogoff()
        {
            this.writeReceiveRtfConversa("saiu.");
            this.IsEnabled = false;
        }

        /// <summary>
        /// método para receber mensagem de login
        /// caso o chatbox estivesse inativo
        /// em que o usuário saiu e entrou novamente
        /// reabilita o chatbox
        /// </summary>
        /// <param name="message"></param>
        public void ReceiveMessageLogin()
        {
            this.writeReceiveRtfConversa("entrou.");
            this.IsEnabled = true;
        }

        /// <summary>
        /// método para enviar a mensagem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btEnviarMsg_Click(object sender, RoutedEventArgs e)
        {
            MessageSendMessage m = new MessageSendMessage();
            m.From = From;
            m.To = To;
            m.MessageText = txtConversa.Text;

            TipoOperacao.SendMessageTo(m);

            writeSendRtfConversa("diz: ", txtConversa.Text);
            txtConversa.Clear();
        }

        /// <summary>
        /// método para verificar as teclas apertadas no txtConversa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtConversa_KeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.btEnviarMsg_Click(sender, null);
            }
        }

        /// <summary>
        /// método para enviar arquivo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btEnviarArquivo_Click(object sender, RoutedEventArgs e)
        {
            // objeto SaveFileDialog
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.FilterIndex = 0;
            openFileDialog.RestoreDirectory = true;
            //saveFileDialog.CreatePrompt = true;
            openFileDialog.Title = "Enviar arquivo";
            openFileDialog.ShowDialog();
            if (openFileDialog.FileName != "")
            {
                // ???????
                if (new FileInfo(openFileDialog.FileName).Length > TypeOperation.capacityByte / 5 * 4)
                {
                    MessageBox.Show("O arquivo deverá ser menor que 4 MB", "Erro");
                    return;
                }
                MessageSendFile m = new MessageSendFile();
                m.From = From;
                m.To = To;
                m.FileName = openFileDialog.FileName;
                m.File = File.ReadAllBytes(openFileDialog.FileName);

                try
                {
                    TipoOperacao.SendFileTo(m);
                }
                catch (Exception ex)
                {
                    writeSendRtfConversa(ex.Message);
                    txtConversa.Clear();
                    return;
                }
                writeSendRtfConversa("enviou o arquivo  " + m.FileName);
                txtConversa.Clear();
            }
        }

        /// <summary>
        /// método para escrever rtfConversa quando do envio de mensagem, arquivo, etc...
        /// </summary>
        /// <param name="title"> título da mensagem </param>
        /// <param name="message"> mensagem (opcional) </param>
        private void writeSendRtfConversa(string title, string message=null)
        {
            Paragraph p = new Paragraph();
            p.Margin = new Thickness(0);
            Inline in1 = new Bold(new Run(DateTime.Now.ToString(@"(HH:mm:ss) ") + From.NomeUsuario + " " + title));
            in1.Foreground = Brushes.Blue;
            p.Inlines.Add(in1);
            if(message != null)
            {
                p.Inlines.Add(new Run(message));
            }
            rtfConversa.Document.Blocks.Add(p);
            rtfConversa.ScrollToEnd();
        }

        /// <summary>
        /// método para escrever rtfConversa quando do recebimento de mensagem, arquivo, notificação, etc...
        /// </summary>
        /// <param name="title">título da mensagem </param>
        /// <param name="message"> mensagem (opcional) </param>
        private void writeReceiveRtfConversa(string title, string message = null)
        {
            Paragraph p = new Paragraph();
            p.Margin = new Thickness(0);
            Inline in1 = new Bold(new Run(DateTime.Now.ToString(@"(HH:mm:ss) ") + To.NomeUsuario + " " + title));
            in1.Foreground = Brushes.Red;
            p.Inlines.Add(in1);
            if (message != null)
            {
                p.Inlines.Add(new Run(message));
            }
            rtfConversa.Document.Blocks.Add(p);
            rtfConversa.ScrollToEnd();
        }
    }
}
