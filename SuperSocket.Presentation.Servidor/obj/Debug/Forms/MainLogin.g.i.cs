﻿#pragma checksum "..\..\..\Forms\MainLogin.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "7693CD2ED811DB724529AB12B8507EB6"
//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.42000
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------

using SuperSocket.Presentation.Servidor.Forms;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SuperSocket.Presentation.Servidor.Forms {
    
    
    /// <summary>
    /// MainLogin
    /// </summary>
    public partial class MainLogin : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 1 "..\..\..\Forms\MainLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SuperSocket.Presentation.Servidor.Forms.MainLogin frmLogin;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\Forms\MainLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlock;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\Forms\MainLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbServidor;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\Forms\MainLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbCliente;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\Forms\MainLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtUsuario;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\Forms\MainLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbIP;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Forms\MainLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtIP;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\Forms\MainLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbPorta;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Forms\MainLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPorta;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\Forms\MainLogin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btEntrar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SuperSocket.Presentation.Servidor;component/forms/mainlogin.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Forms\MainLogin.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.frmLogin = ((SuperSocket.Presentation.Servidor.Forms.MainLogin)(target));
            
            #line 8 "..\..\..\Forms\MainLogin.xaml"
            this.frmLogin.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.frmLogin_MouseLeftButtonDown);
            
            #line default
            #line hidden
            
            #line 9 "..\..\..\Forms\MainLogin.xaml"
            this.frmLogin.Loaded += new System.Windows.RoutedEventHandler(this.MainLogin_Load);
            
            #line default
            #line hidden
            return;
            case 2:
            this.textBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.rbServidor = ((System.Windows.Controls.RadioButton)(target));
            
            #line 29 "..\..\..\Forms\MainLogin.xaml"
            this.rbServidor.Checked += new System.Windows.RoutedEventHandler(this.rb_select_checked);
            
            #line default
            #line hidden
            return;
            case 4:
            this.rbCliente = ((System.Windows.Controls.RadioButton)(target));
            
            #line 30 "..\..\..\Forms\MainLogin.xaml"
            this.rbCliente.Checked += new System.Windows.RoutedEventHandler(this.rb_select_checked);
            
            #line default
            #line hidden
            return;
            case 5:
            this.txtUsuario = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.lbIP = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.txtIP = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.lbPorta = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.txtPorta = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.btEntrar = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\..\Forms\MainLogin.xaml"
            this.btEntrar.Click += new System.Windows.RoutedEventHandler(this.btEntrar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

