﻿using SocketUniversal.Domain;
using SocketUniversal.SocketServidor.Util;
using SocketUniversal.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketUniversal.Sockets
{
    // Este delegate é necessário para especificar os parametros que estamos pasando com o nosso evento
    public delegate void StatusChangedEventHandler(object sender, StatusChangedEventArgs e);

    public delegate void StatusChangedEventHandlerAlert(object sender, StatusChangedEventArgs e);

    public delegate void StatusChangedEventHandlerExit(object sender, StatusChangedEventArgs e);

    public delegate void MessageEventHandler(object sender, MessageEventArgs eM);

    public abstract class TypeOperation
    {
        public readonly static int capacityByte = 5000 * 1024;

        protected static StatusChangedEventArgs e;

        protected static MessageEventArgs eM;

        // O evento e o seu argumento irá notificar o formulário quando um usuário se conecta, desconecta, envia uma mensagem,etc
        public static event StatusChangedEventHandler StatusChanged;

        public static event StatusChangedEventHandlerAlert StatusChangedAlert;

        public static event StatusChangedEventHandlerExit StatusChangedExit;

        public static event MessageEventHandler MessageSender;

        // Este evento é chamado quando queremos disparar o evento StatusChanged
        public static void OnStatusChanged(StatusChangedEventArgs e)
        {
            StatusChanged?.Invoke(null, e);
        }

        // Este evento é chamado quando queremos disparar o evento StatusChanged
        public static void OnStatusChangedAlert(StatusChangedEventArgs e)
        {
            StatusChangedAlert?.Invoke(null, e);
        }

        public static void OnStatusChangedExit(StatusChangedEventArgs e)
        {
            StatusChangedExit?.Invoke(null, e);
        }

        // Este evento é chamado quando queremos disparar o evento
        public static void OnStatusMessage(MessageEventArgs e)
        {
            MessageSender?.Invoke(null, e);
        }

        public abstract void SendFileTo(MessageSendFile message);

        public abstract void SendMessageTo(MessageSendMessage message);

        /// <summary>
        /// método para retornar ip local
        /// </summary>
        /// <returns> IPAddress ip local </returns>
        public static IPAddress LocalIPAddress()
        {
            // verifica se está conectado a rede
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                return null;
            }

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            return host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }

        /// <summary>
        /// 
        /// http://stackoverflow.com/questions/2661764/how-to-check-if-a-socket-is-connected-disconnected-in-c
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool SocketConnected(Socket s)
        {
            bool part1 = s.Poll(1000, SelectMode.SelectRead);
            bool part2 = (s.Available == 0);
            if (part1 && part2)
                return false;
            else
                return true;
        }

        /// <summary>
        /// método para fechar conexões e sair
        /// </summary>
        public abstract void Exit();
    }
    
}
