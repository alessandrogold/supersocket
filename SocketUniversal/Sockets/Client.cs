﻿using SocketUniversal.Domain;
using SocketUniversal.SocketServidor.Util;
using SocketUniversal.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace SocketUniversal.Sockets
{

    public class Client : TypeOperation
    {
        /// <summary>
        /// usuário client
        /// </summary>
        public User UserClient;
        /// <summary>
        /// usuário servidor
        /// </summary>
        public User UserServer;
        /// <summary>
        /// lista de usuários conectados
        /// </summary>
        private List<User> listUsers;
        /// <summary>
        /// Thread do client
        /// </summary>
        private Thread threadMain;
        /// <summary>
        /// fornece conexão do cliente
        /// </summary>
        private TcpClient tcpClient;
        /// <summary>
        /// fluxo de dados subjacente para acesso à rede
        /// </summary>
        private NetworkStream sockStream;
        /// <summary>
        /// lê tipos de dados como valores binários
        /// </summary>
        private BinaryReader le;
        /// <summary>
        /// atributo para enviar ou receber mensagem em bytes
        /// </summary>
        byte[] byteData = new byte[capacityByte];

        public Client(User userServer, User userClient)
        {
            this.UserServer = userServer;
            this.UserClient = userClient;
            this.listUsers = new List<User>();
            threadMain = new Thread(new ThreadStart(RunClient));
            threadMain.Start();

            // inicializa eventos
            e = new StatusChangedEventArgs();
            eM = new MessageEventArgs();
        }

        /// <summary>
        /// método para conectar
        /// </summary>
        public void RunClient()
        {
            try
            {
                tcpClient = new TcpClient();
                tcpClient.Connect((IPEndPoint)UserServer.EndPoint);

                sockStream = tcpClient.GetStream();
                // Lê tipos de dados primitivos como valores binários em uma codificação específica.
                le = new BinaryReader(sockStream);

                // monta e envia objeto messagelogin
                Message message = new MessageLogin()
                {
                    UserLogin = UserClient
                };

                byteData = message.ToByteArray();
                tcpClient.Client.Send(byteData);

                byteData = new byte[capacityByte];
                do
                {
                    // teste recebimento usuario
                    le.Read(byteData, 0, byteData.Length);

                    var empty = byteData.All(B => B == default(Byte));

                    if (!empty)
                    {
                        message = Message.ToObj(byteData) as Message;
                    }
                    else
                    {
                        break;
                    }

                    if (message.GetType() == typeof(MessageSendMessage)
                        || message.GetType() == typeof(MessageSendFile)
                        || message.GetType() == typeof(MessageLogoff)
                        || message.GetType() == typeof(MessageSendList))
                    {
                        eM.Message = message;
                        OnStatusMessage(eM);
                    }
                    // limpa objetos
                    message = null;
                    Array.Clear(byteData, 0, byteData.Length);
                } while (tcpClient.Connected);
            }
            catch (Exception error)
            {
                e.EventMessage = error.Message;
                OnStatusChangedAlert(e);
            }
            finally
            {
                e.EventMessage = "O aplicativo será fechado.";
                OnStatusChangedAlert(e);
                e.EventMessage = String.Empty;
                OnStatusChangedExit(e);
            }
        }

        /// <summary>
        /// método para enviar mensagem (objeto MessageSendMEsssage)
        /// </summary>
        /// <param name="m"> objeto MessageSendMEsssage </param>
        public override void SendMessageTo(MessageSendMessage m)
        {
            if (tcpClient != null)
            {
                try
                {
                    byte[] byteData = m.ToByteArray();
                    tcpClient.Client.Send(byteData);
                } catch(Exception ex)
                {
                    e.EventMessage = ex.Message;
                    OnStatusChangedAlert(e);
                }
                
            }
        }

        /// <summary>
        /// método para enviar arquivo (objeto MessageSendFile)
        /// </summary>
        /// <param name="m"></param>
        public override void SendFileTo(MessageSendFile m)
        {
            if (tcpClient != null)
            {
                byte[] byteData = m.ToByteArray();
                if (byteData.Length > capacityByte)
                {
                    throw new Exception("Não foi possível enviar o arquivo.");
                }
                try
                {
                    tcpClient.Client.Send(byteData);
                }
                catch (Exception ex)
                {
                    e.EventMessage = ex.Message;
                    OnStatusChangedAlert(e);
                }
            }
        }

        public override void Exit()
        {
            if (le != null)
                le.Close();
            if(sockStream != null)
                sockStream.Close();
            if (tcpClient.Connected)
            {
                tcpClient.GetStream().Close();
                tcpClient.Close();
            }
        }
    }
}
