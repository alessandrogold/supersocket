﻿using SocketUniversal.Domain;
using SocketUniversal.SocketServidor.Util;
using SocketUniversal.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SocketUniversal.Sockets
{

    public class Server : TypeOperation
    {
        /// <summary>
        /// usuário servidor
        /// </summary>
        private User userServer;
        /// <summary>
        /// classe para escutar os clientes
        /// </summary>
        private TcpListener tcpListener;
        /// <summary>
        /// thread principal para rodar TcpListener
        /// </summary>
        private Thread threadMain;
        /// <summary>
        /// dicionário com usuário e TcpClient 
        /// </summary>
        private Dictionary<User, TcpClient> listUsers;

        public Server(User _user)
        {
            this.userServer = _user;
            this.listUsers = new Dictionary<User, TcpClient>();
            // thread para aceitar multiplas conexões
            threadMain = new Thread(new ThreadStart(RunServer));
            threadMain.Start();

            // inicializa eventos
            e = new StatusChangedEventArgs();
            eM = new MessageEventArgs();
        }

        /// <summary>
        /// método para rodar o servidor
        /// </summary>
        public void RunServer()
        {
            try
            {
                tcpListener = new TcpListener((IPEndPoint)userServer.EndPoint);
                tcpListener.Start();

                listUsers.Add(userServer, null);

                var mInitial = new MessageSendList(listUsers.Select(s => s.Key).ToList());
                // envia mensagem para a UI
                eM.Message = mInitial;
                OnStatusMessage(eM);
                // insere no txtLogs
                e.EventMessage = "Aguardando Conexoes...";
                OnStatusChanged(e);

                while (true)
                {
                    //Repetição até que um cliente se conecte ao servidor
                    //Aceita uma solicitação de conexão pendente
                    TcpClient client = tcpListener.AcceptTcpClient();
                    // cria thread para o cliente que se colectou
                    Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
                    clientThread.Start(client);
                }
            }
            catch (SocketException errorS)
            {
                e.EventMessage = errorS.Message;
                OnStatusChangedAlert(e);
            }
            catch (Exception error)
            {
                e.EventMessage = error.Message;
                OnStatusChangedAlert(e);
            }
            finally
            {
                tcpListener.Stop();
            }
        }

        /// <summary>
        /// controle por cliente conectado
        /// </summary>
        /// <param name="client">objeto TcpClient</param>
        private void HandleClientComm(object client)
        {
            // Fornece conexões do cliente de serviços de rede TCP.
            TcpClient tcpClient = (TcpClient)client;
            // Fornece o fluxo de dados subjacente para acesso à rede.
            NetworkStream clientStream = tcpClient.GetStream();
            // Lê tipos de dados primitivos como valores binários em uma codificação específica.
            BinaryReader le = new BinaryReader(clientStream);

            byte[] byteData = new byte[capacityByte];

            User user = null;

            Message message = null;

            do
            {
                try
                {
                    le.Read(byteData, 0, byteData.Length);
                    // verifica se variável byteData está vazia
                    var empty = byteData.All(B => B == default(Byte));

                    if (!empty)
                    {
                        message = Message.ToObj(byteData) as Message;
                    } else
                    {
                        break;
                    }
                    
                    if (message.GetType() == typeof(MessageLogin))
                    {
                        MessageLogin messageLogin = message as MessageLogin;
                        user = messageLogin.UserLogin;

                        listUsers.Add(user, tcpClient);

                        MessageSendList m = new MessageSendList() {
                            ListUsers = listUsers.Select(s => s.Key).ToList()
                        };
                        eM.Message = m;
                        OnStatusMessage(eM);

                        // informa que conexão foi recebida
                        e.EventMessage = user.NomeUsuario + " entrou.";
                        OnStatusChanged(e);
                        // envia lista a todos os usuários conectados
                        this.SendList();

                    } else if(message.GetType() == typeof(MessageSendMessage))
                    {
                        MessageSendMessage messageReceive = message as MessageSendMessage;
                        // verifica se a mensagem é endereçada ao server
                        if (messageReceive.To.Equals(userServer))
                        {
                            eM.Message = messageReceive;
                            OnStatusMessage(eM);
                        } else
                        {
                            TcpClient tcpClientTo = listUsers[messageReceive.To];
                            if(tcpClientTo != null)
                            {
                                byteData = messageReceive.ToByteArray();
                                tcpClientTo.Client.Send(byteData, byteData.Length, SocketFlags.None);
                            }

                        }

                    }
                    else if (message.GetType() == typeof(MessageSendFile))
                    {
                        MessageSendFile messageReceive = message as MessageSendFile;

                        if (messageReceive.To.Equals(userServer))
                        {
                            eM.Message = messageReceive;
                            OnStatusMessage(eM);
                        }
                        else
                        {
                            TcpClient tcpClientTo = listUsers[messageReceive.To];
                            if (tcpClientTo != null)
                            {
                                byteData = messageReceive.ToByteArray();
                                tcpClientTo.Client.Send(byteData, byteData.Length, SocketFlags.None);
                            }

                        }

                    }
                    // limpa objetos
                    message = null;
                    Array.Clear(byteData, 0, byteData.Length);
                }
                catch (Exception ex)
                {
                    e.EventMessage = user.NomeUsuario + ": " +  ex.ToString();
                    OnStatusChanged(e);
                    break;
                }

            } while (tcpClient.Connected);
            
            listUsers.Remove(user);
            // envia objeto mensagem com lista de usuários
            eM.Message = new MessageSendList( listUsers.Select(s => s.Key).ToList());
            OnStatusMessage(eM);
            // envia string para o txtlogs
            e.EventMessage = user.NomeUsuario + " saiu.";
            OnStatusChanged(e);

            // reenvia a lista para todos os clientes conectadoss
            this.SendList();

            // envia mensagem de usuário deslogado
            this.SendUserLogoff(user);

            //fechando a conexao
            le.Close();
            clientStream.Close();
            tcpClient.Close();
        }

        /// <summary>
        /// método para enviar lista de usuários online
        /// </summary>
        private void SendList()
        {
            MessageSendList message = new MessageSendList(listUsers.Select(s => s.Key).ToList());
            byte[]  byteData = message.ToByteArray();
            foreach(KeyValuePair<User, TcpClient> item in listUsers.Where(w => w.Key.Opcao == OpcoesUserEnum.Cliente))
            {
                item.Value.Client.Send(byteData, byteData.Length, SocketFlags.None);
            }
        }

        /// <summary>
        /// informa que um usuário saiu do chat
        /// </summary>
        /// <param name="userLogoff"> User </param>
        private void SendUserLogoff(User userLogoff)
        {
            MessageLogoff message = new MessageLogoff() {
                UserLogoff = userLogoff
            };

            byte[] byteData = message.ToByteArray();
            foreach (KeyValuePair<User, TcpClient> item in listUsers.Where(w => w.Key.Opcao == OpcoesUserEnum.Cliente))
            {
                try
                {
                    item.Value.Client.Send(byteData, byteData.Length, SocketFlags.None);
                }
                catch (Exception ex)
                {
                    e.EventMessage = ex.Message;
                    OnStatusChanged(e);
                }
                
            }
            // envia mensagem para interface gráfica do server
            eM.Message = message;
            OnStatusMessage(eM);
        }

        /// <summary>
        /// método para enviar mensagem de texo
        /// </summary>
        /// <param name="m">objeto MessageSendMessage</param>
        public override void SendMessageTo(MessageSendMessage m)
        {
            TcpClient tcpClientTo = listUsers[m.To];
            if(tcpClientTo != null)
            {
                byte[] byteData = m.ToByteArray();
                try
                {
                    tcpClientTo.Client.Send(byteData, byteData.Length, SocketFlags.None);
                } catch(Exception ex)
                {
                    e.EventMessage = ex.Message;
                    OnStatusChanged(e);
                }
                
            }
        }

        /// <summary>
        /// método para enviar arquivo
        /// </summary>
        /// <param name="m">objeto MessageSendFile</param>
        public override void SendFileTo(MessageSendFile m)
        {
            TcpClient tcpClientTo = listUsers[m.To];
            if (tcpClientTo != null)
            {
                byte[] byteData = m.ToByteArray();
                if (byteData.Length > capacityByte)
                {
                    throw new Exception("Não foi possível enviar o arquivo.");
                }
                try
                {
                    tcpClientTo.Client.Send(byteData, byteData.Length, SocketFlags.None);
                }
                catch (Exception ex)
                {
                    e.EventMessage = ex.Message;
                    OnStatusChangedAlert(e);
                }
            }
        }

        public override void Exit()
        {
            SendUserLogoff(userServer);
            tcpListener.Stop();
        }
    }
}
