﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SocketUniversal.Domain
{
    [Serializable]
    public class MessageSendFile : Message
    {
        /// <summary>
        /// usuário que transmite
        /// </summary>
        public User From { get; set; }
        /// <summary>
        /// destinatário da mensagem
        /// </summary>
        public User To { get; set; }
        /// <summary>
        /// nome do arquivo
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// arquivo em bytes
        /// </summary>
        public byte[] File { get; set; }

        public MessageSendFile() { }

        public MessageSendFile(byte[] data)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            MessageSendFile obj = (MessageSendFile)binForm.Deserialize(memStream);
            this.From = obj.From;
            this.To = obj.To;
            this.FileName = obj.FileName;
            this.File = obj.File;
        }
    }
}
