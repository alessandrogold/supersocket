﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SocketUniversal.Domain
{
    [Serializable]
    public class User : IComparable<User>, INotifyPropertyChanged
    {
        private int id;
        /// <summary>
        /// Id identificador do usuário
        /// </summary>
        public int Id {
            get {return id; }
            set
            {
                id = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Id)));
            }
        }
        private string nomeUsuario;
        /// <summary>
        /// nome do usuário
        /// </summary>
        public string NomeUsuario {
            get { return nomeUsuario; }
            set{
                nomeUsuario = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(NomeUsuario)));
            }
        }
        private EndPoint endPoint;
        /// <summary>
        /// Socket do client
        /// </summary>
        public EndPoint EndPoint {
            get { return endPoint; }
            set
            {
                endPoint = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EndPoint)));
            }
        }
        private OpcoesUserEnum opcao;
        public OpcoesUserEnum Opcao {
            get { return opcao; }
            set
            {
                opcao = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Opcao)));

            }
        }

        public User()
        {
        }

        public User(string nomeUsuario, OpcoesUserEnum opcao, EndPoint endPoint)
        {
            this.NomeUsuario = nomeUsuario;
            this.Opcao = opcao;
            this.EndPoint = endPoint;
        }

        // anotação que avisa para não serializar atributo
        [field:NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        public String DisplayMember()
        {
            return NomeUsuario;
        }

        public byte[] ToByteArray()
        {
            if (this == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, this);
                return ms.ToArray();
            }
        }

        public User(byte[] data)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            User obj = (User)binForm.Deserialize(memStream);
            this.Id = obj.Id;
            this.NomeUsuario = obj.NomeUsuario;
            this.EndPoint = obj.EndPoint;
            this.Opcao = obj.Opcao;
        }

        public static byte[] SerializeList(IEnumerable<User> items)
        {
            var binFormatter = new BinaryFormatter();
            var mStream = new MemoryStream();
            binFormatter.Serialize(mStream, items);

            return mStream.ToArray();
        }
    

        public static List<User> DeserializeList(byte[] byteData)
        {
            var mStream = new MemoryStream();
            var binFormatter = new BinaryFormatter();

            // Where 'objectBytes' is your byte array.
            mStream.Write(byteData, 0, byteData.Length);
            mStream.Position = 0;

            return binFormatter.Deserialize(mStream) as List<User>;
        }

        public int CompareTo(User obj)
        {
            return (Id + NomeUsuario + Opcao + EndPoint.ToString())
                .CompareTo(obj.Id + obj.NomeUsuario + obj.Opcao + obj.EndPoint.ToString());
        }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
                return false;

            User user = obj as User;
            if ((System.Object)user == null)
                return false;

            return (Id == user.Id) 
                && (NomeUsuario == user.NomeUsuario) 
                && (EndPoint.ToString() == user.EndPoint.ToString()) 
                && Opcao == user.Opcao;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^ NomeUsuario.GetHashCode() ^ Opcao.ToString().GetHashCode() ^ EndPoint.ToString().GetHashCode();
        }
    }
}
