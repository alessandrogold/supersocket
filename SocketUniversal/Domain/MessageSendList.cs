﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SocketUniversal.Domain
{
    [Serializable]
    public class MessageSendList : Message
    {
        public List<User> ListUsers { get; set; }

        public MessageSendList()
        {
            ListUsers = new List<User>();
        }

        public MessageSendList(List<User> listUsers)
        {
            this.ListUsers = listUsers;
        }

        public MessageSendList(byte[] data)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            MessageSendList obj = (MessageSendList)binForm.Deserialize(memStream);
            this.ListUsers = obj.ListUsers;
        }
    }
}
