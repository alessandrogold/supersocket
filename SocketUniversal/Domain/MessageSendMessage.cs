﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SocketUniversal.Domain
{
    [Serializable]
    public class MessageSendMessage : Message
    {
        /// <summary>
        /// usuário que transmite
        /// </summary>
        public User From { get; set; }
        /// <summary>
        /// destinatário da mensagem
        /// </summary>
        public User To { get; set; }
        /// <summary>
        /// mensagem transmitida
        /// </summary>
        public string MessageText { get; set; }

        /// <summary>
        /// data de envio da mensagem
        /// </summary>
        public DateTime Date { get; }

        public MessageSendMessage()
        {
            Date = DateTime.Now;
        }

        public MessageSendMessage(byte[] data)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            MessageSendMessage obj = (MessageSendMessage)binForm.Deserialize(memStream);
            this.From = obj.From;
            this.To = obj.To;
            this.MessageText = obj.MessageText;
            this.Date = obj.Date;
        }
    }
}
