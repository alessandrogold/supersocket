﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SocketUniversal.Domain
{
    [Serializable]
    public class MessageLogin : Message
    {
        public User UserLogin { get; set; }

        public MessageLogin() { }

        public MessageLogin(byte[] data)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            MessageLogin obj = binForm.Deserialize(memStream) as MessageLogin;
            this.UserLogin = obj.UserLogin;
        }
    }
}
