﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SocketUniversal.Domain
{
    [Serializable]
    public class MessageLogoff : Message
    {
        public User UserLogoff { get; set; }

        public MessageLogoff() { }

        public MessageLogoff(byte[] data)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            MessageLogoff obj = binForm.Deserialize(memStream) as MessageLogoff;
            this.UserLogoff = obj.UserLogoff;
        }
    }
}
