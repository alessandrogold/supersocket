﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketUniversal.Domain
{
    [Serializable]
    public enum OpcoesUserEnum
    {
        Servidor = 0,
        Cliente = 1
    }
}
