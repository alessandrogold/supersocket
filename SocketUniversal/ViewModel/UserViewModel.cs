﻿using SocketUniversal.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketUniversal.ViewModel
{
    public class UserViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<User> users;

        public ObservableCollection<User> Users
        {
            get
            {
                return users;
            }
            set
            {
                users = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Users)));
            }
        }

        public UserViewModel()
        {
            Users = new ObservableCollection<User>();
        }

        public void UpdateList(List<User> listUsers)
        {
            Users.Clear();
            foreach (User user in listUsers)
            {
                Users.Add(user);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
