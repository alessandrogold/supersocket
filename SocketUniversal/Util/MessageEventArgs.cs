﻿using SocketUniversal.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketUniversal.Util
{
    /// <summary>
    /// evento para passar objeto message
    /// </summary>
    public class MessageEventArgs : EventArgs
    {
        // Propriedade para retornar e definir um mensagem
        public Message Message { get; set; }

        // Construtor para definir a mensagem
        public MessageEventArgs()
        {
        }

        public MessageEventArgs(Message message)
        {
            this.Message = message;
        }
    }
}
