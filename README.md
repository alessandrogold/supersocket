# SuperSocket

## Descri��o

Software, desenvolvido na linguagem de programa C#, de chat numa rede local para a disciplina Laborat�rio de Redes, do curso An�lise e Desenvolvimento de Sistemas, da Faculdade de Tecnologia SENAC Pelotas.

Permite o envio e recebimento de mensagens e arquivos.

Utiliza interface gr�fica WPF.